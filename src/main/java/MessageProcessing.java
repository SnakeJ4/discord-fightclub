import fighting.FighterBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.util.Scanner;

public class MessageProcessing extends ListenerAdapter {

    private final String key = "!*";
    private final String admin = "326415273827762176";
    private MessageChannel channel;
    private Guild guild;
    private User user;
    private String message;

    private final FighterBuilder fighterBuilder = new FighterBuilder();
    private FighterBuilder.Fighter attacker;
    private FighterBuilder.Fighter defender;
    private MessageChannel fightChannel;
    private boolean fightInProgress = false;

    private Grind grind;

    private int yoted = 0;

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        channel = event.getChannel();
        guild = event.getGuild();
        user = event.getAuthor();

        if(guild.getName().equals("Life of a Mop")){
            String fileName = channel.getName() + ".txt";
            FileWriter file = null;
            try {
                file = new FileWriter("src/main/java/Resources/spying/" + fileName, true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            BufferedWriter bufferedWriter = new BufferedWriter(file);
            PrintWriter writer = new PrintWriter(bufferedWriter);
            writer.println("From : " + user.getName() + " : " + event.getMessage().getContentRaw());
            writer.close();
        }
        if (event.getMessage().getContentRaw().indexOf("!*") == 0) {
            message = event.getMessage().getContentRaw().substring(2).toLowerCase();
            switch(message){
                case "ping":
                    sendMessage("pong!");
                    break;
                case "yeet":
                    yoted++;
                    sendMessage("I have been yoted on " + yoted + " times.");
                    break;
                case "oof":
                    sendMessage("oofn't");
                    break;
                case "stopgrind":
                    if(user.getId().equals(admin)){
                        grind.stop();
                        sendMessage("Grinding has ended");
                    }
                    break;
                case "stop fight":
                case "stopfight":
                    if(fightInProgress && (user == attacker.getUser() || user == defender.getUser())){
                        fightInProgress = false;
                        sendMessage("The current fight has been stopped");
                    } else {
                        sendMessage("You must be a participant in a fight that is currently happening");
                    }
                    break;
                case "stopmeme":
                    FileWriter file = null;
                    try {
                        file = new FileWriter("src/main/java/Resources/dontmeme.txt", true);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    BufferedWriter bufferedWriter = new BufferedWriter(file);
                    PrintWriter writer = new PrintWriter(bufferedWriter);
                    writer.println(user.getId());
                    writer.close();
            }
            if(message.contains(" ")){
                switch(message.substring(0, message.indexOf(" "))){
                    case "sendmessage":
                        if(user.getId().equals(admin)){
                            sendMessage(message.substring(message.indexOf(" ") + 1));
                        } else {
                            sendMessage("You must be an admin to use this command");
                        }
                        break;
                    case "setitem":
                        FighterBuilder.Fighter modFighter = fighterBuilder.generateFighter(user);
                        sendMessage("Your item has been set to: " + modFighter.changeItem(message.substring(message.indexOf(" ") + 1).toLowerCase()) + " If this isn't the item you wanted you may not own this item");
                        break;
                    case "fight":
                        if (!fightInProgress) {
                            startFight();
                        } else {
                            sendMessage("Please wait until the current fight is over <@" + user.getId() + ">");
                        }
                        break;
                    case "grind":
                        if(user.getId().equals(admin)) {
                            if (message.contains("!")) {
                                grind = new Grind(channel, guild.getMemberById(message.substring(message.indexOf("@") + 2, message.lastIndexOf(">"))).getUser());
                            } else {
                                grind = new Grind(channel, guild.getMemberById(message.substring(message.indexOf("@") + 1, message.lastIndexOf(">"))).getUser());
                            }
                            grind.start();
                            sendMessage("The grind has begun :smiling_imp:");
                        } else {
                            sendMessage("You don't have the right permissions to use this command");
                        }
                        break;
                }
            }
        } else if (fightInProgress && user == attacker.getUser() && channel == fightChannel) {
            String move = event.getMessage().getContentRaw().toLowerCase();
            int damageDealt = attacker.getDamage(move);
            if(damageDealt == -1){
                sendMessage("You can't use that move!");
                return;
            } else if (damageDealt <= -2){
                sendMessage("This move is't charged yet! Wait " + (Math.abs(damageDealt) + 1) + " turns!");
            }
        } else if (event.getMessage().getContentRaw().toLowerCase().contains("dab") && !checkMeme(user.getId())){
            try {
                Image userPhoto = ImageIO.read(new URL(user.getAvatarUrl()));
                Image dabArms = ImageIO.read(new File("src/main/java/Resources/DAB.png"));

                // create the new image, canvas size is the max. of both image sizes
                int w = Math.max(userPhoto.getWidth(null), dabArms.getWidth(null));
                int h = Math.max(userPhoto.getHeight(null), dabArms.getHeight(null));
                BufferedImage combined = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

// paint both images, preserving the alpha channels
                Graphics g = combined.getGraphics();
                g.drawImage(userPhoto, 82, 41, null);
                g.drawImage(dabArms, 0, 0, null);

                File outputFile = new File("src/main/java/Resources/dankdaber.png");
                ImageIO.write(combined, "png", outputFile);
                channel.sendFile(new File("src/main/java/Resources/dankdaber.png")).queue();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if(event.getMessage().getContentRaw().toLowerCase().equals("oof") && !checkMeme(user.getId())){
            try {
                Image userPhoto = ImageIO.read(new URL(user.getAvatarUrl()));
                Image oofBack = ImageIO.read(new File("src/main/java/Resources/oofBack.png"));

                // create the new image, canvas size is the max. of both image sizes
                int w = Math.max(userPhoto.getWidth(null), oofBack.getWidth(null));
                int h = Math.max(userPhoto.getHeight(null), oofBack.getHeight(null));
                BufferedImage combined = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

// paint both images, preserving the alpha channels
                Graphics g = combined.getGraphics();
                g.drawImage(oofBack, 0, 0, null);
                g.drawImage(userPhoto, 40, 62, 150, 150,  null);

                File outputFile = new File("src/main/java/Resources/oof.png");
                ImageIO.write(combined, "png", outputFile);
                channel.sendFile(new File("src/main/java/Resources/oof.png")).queue();
                channel.deleteMessageById(event.getMessageId()).queue();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if(event.getMessage().getContentRaw().toLowerCase().equals("yeet") && !checkMeme(user.getId())){
            try {
                Image userPhoto = ImageIO.read(new URL(user.getAvatarUrl()));
                Image yeetBack = ImageIO.read(new File("src/main/java/Resources/yeetBack.png"));

                // create the new image, canvas size is the max. of both image sizes
                int w = Math.max(userPhoto.getWidth(null), yeetBack.getWidth(null));
                int h = Math.max(userPhoto.getHeight(null), yeetBack.getHeight(null));
                BufferedImage combined = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

// paint both images, preserving the alpha channels
                Graphics g = combined.getGraphics();
                g.drawImage(yeetBack, 0, 0, null);
                g.drawImage(userPhoto, 100, 95, 90, 90,  null);
                g.drawImage(userPhoto, 480, 140, 90, 90, null);

                File outputFile = new File("src/main/java/Resources/yeet.png");
                ImageIO.write(combined, "png", outputFile);
                channel.sendFile(new File("src/main/java/Resources/yeet.png")).queue();
                channel.deleteMessageById(event.getMessageId()).queue();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean checkMeme(String id) {
        Scanner scan = null;
        try {
            scan = new Scanner(new File("src/main/java/Resources/dontmeme.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while(scan.hasNextLine()){
            if(scan.nextLine().equals(id))
                return true;
        }
        return false;
    }

    public void sendMessage(String message) {
        sendMessage(message, channel);
    }

    public void sendMessage(String message, MessageChannel channel) {
        channel.sendMessage(message).queue();
    }

    private void startFight() {
        String toFight = message.substring(message.indexOf(" ") + 1);
        User defenderUser;
        if (!message.contains("!")) {
            defenderUser = guild.getMemberById(toFight.substring(2, toFight.length() - 1)).getUser();
        } else {
            defenderUser = guild.getMemberById(toFight.substring(3, toFight.length() - 1)).getUser();
        }

        if (defenderUser.isBot()) {
            sendMessage("As you put up your fists to fight the bot that towers above you its brings out its \"fists\" four mechanized saw blades that instantly whir towards you shredding you to pieces in mere seconds before you even get a chance to move.");
            return;
        }
        if (user.isBot()) {
            sendMessage("You stare down your frail fleshy opponent and in an instant your saws have diced their body into chunks later to be consumed as delectable sushi");
            return;
        }
        if (user == defenderUser) {
            sendMessage("Why? Just what do you think beating yourself up is going to do? If there was an xp system I would take some of yours away.");
            return;
        }

        attacker = fighterBuilder.generateFighter(user);
        defender = fighterBuilder.generateFighter(defenderUser);
        fightChannel = channel;
        fightInProgress = true;

        sendMessage("It's <@" + attacker.getId() + ">'s turn. Will they use " + attacker.compileMoves(), fightChannel);
    }
}
