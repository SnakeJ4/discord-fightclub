import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

public class Grind extends Thread {
    public boolean stop = false;

    private final MessageChannel channel;
    private final User grindOn;

    public Grind(MessageChannel channel, User grindOn) {
        this.channel = channel;
        this.grindOn = grindOn;
    }

    @Override
    public void run() {
        while (true) {
            channel.sendMessage("??fight <@" + grindOn.getId() + ">").queue();
            try {
                java.util.concurrent.TimeUnit.SECONDS.sleep(15);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
