package fighting;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Item {
    public static String none = "none";
    public static String over9000 = "over9000";
    public static String shortsword = "shortsword";

    private final String name;
    private final Random rand = new Random();
    private final ArrayList<String> moves = new ArrayList<>();
    private final ArrayList<Integer> moveDamage = new ArrayList<>();
    private final ArrayList<Integer> moveMod = new ArrayList<>();
    private final ArrayList<Integer> moveIsCharged = new ArrayList<>();
    private final ArrayList<Integer> moveRecharge = new ArrayList<>();

    public Item(String itemName){
        Scanner scan = null;
        try {
            scan = new Scanner(new File("src/main/java/fighting/Items/" + itemName + ".txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String identityLine = scan.nextLine();
        name = identityLine.split(",")[0].split(":")[1];
        int attacks = Integer.parseInt(identityLine.split(",")[1].split(":")[1]);
        for(int i = 0; i < attacks; i++){
            String[] move = scan.nextLine().split(",");
            moves.add(move[0].split(":")[1].toLowerCase());
            moveDamage.add(Integer.parseInt(move[1].split(":")[1]));
            moveMod.add(Integer.parseInt(move[2].split(":")[1]));
            moveIsCharged.add(0);
            moveRecharge.add(Integer.parseInt(move[3].split(":")[1]));
        }
    }

    public ArrayList<String> getMoves(){
        return moves;
    }

    public String getName(){ return name; }

    public int getDamage(String move){
        int position = moves.indexOf(move);
        if(position < 0){
            return -1;
        }
        if(moveIsCharged.get(position) != -1){
            return -2;
        }
        return moveDamage.get(position) + rand.nextInt(moveMod.get(position));
    }
}
