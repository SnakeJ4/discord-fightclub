package fighting;

import net.dv8tion.jda.core.entities.User;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class FighterBuilder {

    public Fighter generateFighter(User user){
        try {
            Scanner scan = new Scanner(new File("src/main/java/fighting/Users/default.txt".replaceAll("default", user.getId())));
            String[] pieces = scan.nextLine().split(",");
            String hp = pieces[1].split(":")[1];
            String item = pieces[2].split(":")[1];
            String damageMod = pieces[3].split(":")[1];
            String exp = pieces[4].split(":")[1];
            return new Fighter(Integer.parseInt(hp), item, Integer.parseInt(damageMod), Integer.parseInt(exp), user);
        } catch (FileNotFoundException e){
            createUserFile(user);
            return generateFighter(user);
        }
    }

    private void createUserFile(User user){
        try {
            Scanner defaultReader = new Scanner(new File("src/main/java/fighting/Users/default.txt"));
            String defaultText = defaultReader.nextLine();
            FileOutputStream newFighterFile = new FileOutputStream("src/main/java/fighting/Users/default.txt".replaceAll("default", user.getId()));
            newFighterFile.write(("name:" + user.getName() + "," + defaultText).getBytes());
            newFighterFile.close();
            defaultReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    public class Fighter {

        private final User user;
        private int health;
        private int exp;
        private int damageMod;
        private Item item;
        private String name;

        private Fighter(int health, String itemString, int damageMod, int exp, User user) {
            this.user = user;
            this.name = user.getName();
            this.health = health;
            this.damageMod = damageMod;
            this.exp = exp;
            setItem(itemString);
        }

        private String setItem(String itemString){
            switch (itemString) {
                case "shortsword":
                    this.item = new Item(Item.shortsword);
                    break;
                case "over9000":
                    if (user.getId().equals("326415273827762176")) {
                        this.item = new Item(Item.over9000);
                    } else {
                        this.item = new Item(Item.none);
                    }
                    break;
                default:
                    this.item = new Item(Item.none);
                    break;
            }
            return this.item.getName();
        }

        private void writeItem(String itemString){
            try{
                Scanner scan = new Scanner(new File("src/main/java/fighting/Users/default.txt".replaceAll("default", user.getId())));
                String[] pieces = scan.nextLine().split(",");
                String hp = pieces[1].split(":")[1];
                String damageMod = pieces[3].split(":")[1];
                String exp = pieces[4].split(":")[1];
                FileOutputStream newFighterFile = new FileOutputStream("src/main/java/fighting/Users/default.txt".replaceAll("default", user.getId()));
                newFighterFile.write(("name:" + user.getName() + ",hp:" + hp + ",item:" + itemString + ",damageMod:" + damageMod + ",exp:" + exp).getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public String changeItem(String itemString){
            String setItem = setItem(itemString);
            writeItem(setItem);
            return setItem;
        }

        public void takeDamage(int damage) {
            health = health - damage < 0 ? 0 : health - damage;
        }

        public int getHealth() {
            return health;
        }

        public int getDamageMod() {
            return damageMod;
        }

        public String getName() {
            return name;
        }

        public String getItemName() {
            return item.getName();
        }

        private ArrayList<String> getMoves() {
            return item.getMoves();
        }

        public int getDamage(String move) {
            int damage = item.getDamage(move);
            return damage < 0 ? damage : damage + damageMod;
        }

        public User getUser(){
            return user;
        }

        public String getId(){
            return user.getId();
        }

        public String compileMoves() {
            String compiledMoves = "!@#";
            for(int i = 0; i < getMoves().size(); i++){
                if(i != getMoves().size() - 1){
                    compiledMoves += ", " + getMoves().get(i);
                } else {
                    compiledMoves += " or " + getMoves().get(i);
                }
            }
            compiledMoves.replace("!@#, ", "");
            return compiledMoves;
        }
    }

}
